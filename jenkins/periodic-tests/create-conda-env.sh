#!/bin/bash
###############################################################################
# (c) Copyright 2016-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Installing conda (based on https://indico.cern.ch/event/773049/contributions/3473243/attachments/1938659/3213612/2019-11-05_CHEP2019-conda.pdf)
wget -nv http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
bash miniconda.sh -b -u -p ./miniconda
source ./miniconda/etc/profile.d/conda.sh
conda config --add channels conda-forge

# Remove environment if exists
conda remove -y --name handlers-env --all

# Creating conda environment
conda env create -f handlers_environment.yml
conda activate handlers-env

# Conda and pip are each aware of packages installed by the other
pip install LbNightlyTools

# Hack to prevent ssl.SSLCertVerificationError: [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: unable to get local issuer certificate (_ssl.c:1108)
export SSL_CERT_DIR=/etc/pki/tls/certs/ 

# Pack environment
conda install conda-pack
conda-pack -f

# Publish to eos
echo "Publishing handlers environment to /eos/lhcb/storage/lhcbpr/conda/handlers-env.tar.gz"
xrdcp -f handlers-env.tar.gz root://eosuser.cern.ch//eos/lhcb/storage/lhcbpr/conda/handlers-env.tar.gz

# Deactivate environment
conda deactivate
